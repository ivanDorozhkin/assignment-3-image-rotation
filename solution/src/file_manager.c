#include "file_manager.h"


FILE* file_reader(char* name){
    return (fopen(name, "rb"));
}

FILE* file_writer(char* name){
    return (fopen(name, "wb"));
}

void close_file(FILE* file){
    fclose(file);
}

size_t read_from_file(void* data, size_t size, size_t count, FILE* file){
    return fread(data, size, count, file);
}

size_t write_to_file(void* data, size_t size, size_t count, FILE* file){
    return fwrite(data, size, count, file);
}
