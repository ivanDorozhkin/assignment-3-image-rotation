#include "stdio.h"
#include "bmp.h"
#include "file_manager.h"

#include "rotate.h"
#include "stdlib.h"

void error_in_program(char* error_message){
    fprintf(stderr, "%s", error_message);
    exit(1);
}

int main(int argc, char**argv) {
    if (argc != 3)
        error_in_program("Wrong amount of arguments.");

    struct image start = {0};
    struct image result = {0};

    FILE* file_in = file_reader(argv[1]);
    if (!file_in || !from_bmp(file_in, &start))
        error_in_program("Read file error");

    result = rotate(start);
    FILE* file_out = file_writer(argv[2]);
    if (!file_out || !to_bmp(file_out, &result))
        error_in_program("Write file error");

    free_image(&start);
    free_image(&result);
    close_file(file_in);
    close_file(file_out);

    printf("Inverted picture is ready!");
    return 0;
}
