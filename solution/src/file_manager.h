#ifndef IMAGE_TRANSFORMER_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_FILE_MANAGER_H

#include <stdio.h>

FILE* file_reader(char* name);
FILE* file_writer(char* name);
void close_file(FILE* file);
size_t read_from_file(void* data, size_t size, size_t count, FILE* file);
size_t write_to_file(void* data, size_t size, size_t count, FILE* file);
#endif
