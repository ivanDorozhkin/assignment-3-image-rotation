#include "bmp.h"

const uint16_t bfType_const_value = 19778;
const uint32_t biSize_const_value = 40;
const uint16_t biPlanes_const_value = 1;
const uint16_t biBitCount_const_value = 24;

bool add_header(struct image const* image, FILE* file) {
    struct bmp_header header = {
            .bfileSize = sizeof(struct bmp_header) + count_padding(image) * image->height,
            .biSizeImage = (image->width + count_padding(image)) * image->height,
            .bOffBits = sizeof(struct bmp_header),
            .biWidth = image->width,
            .biHeight = image->height,
            .bfType = bfType_const_value,
            .biSize = biSize_const_value,
            .biPlanes = biPlanes_const_value,
            .biBitCount = biBitCount_const_value,
    };
    return write_to_file(&header, sizeof(struct bmp_header), 1, file);
}

bool get_header(struct bmp_header* header, FILE* file){
    return (read_from_file(header, sizeof(struct bmp_header), 1, file) == 1);
}


bool add_pixels_row(struct image const* image, const uint64_t row_number, FILE* file){
    return (write_to_file(get_pixel_row(image, row_number), sizeof(struct pixel), image->width, file) == image->width);
}

bool add_padding(struct image const* image, FILE* file){
    int8_t empty = 0;
    for (uint64_t i = 0; i < count_padding(image); i++)
        if (write_to_file(&empty, 1, 1, file) == 0)
            return false;
    return true;
}

bool add_body(struct image const* image, FILE* file) {
    for (uint64_t i = 0; i < image->height; i++)
        if (!add_pixels_row(image, i, file) || !add_padding(image, file))
            return false;
    return true;
}

bool get_body(struct image const* image, FILE* file){
    for (size_t i = 0; i < image->height; i++)
        if (read_from_file(get_pixel_row(image, i), sizeof(struct pixel), image->width, file) != image->width ||
            fseek(file, count_padding(image), 1) != 0)
            return false;
    return true;
}

bool from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    bool result = get_header(&header, in);
    if (!result)
        return false;
    *img = create_image(header.biWidth, header.biHeight);
    return get_body(img, in);
}

bool to_bmp( FILE* out, struct image const* img ){
    return add_header(img, out) && add_body(img, out);
}

uint8_t count_padding(struct image const* image){
    return ((4 - ((image->width * sizeof(struct pixel)) % 4)) % 4);
}
