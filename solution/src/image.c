#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
}

void free_image(struct image *image) {
    free(image->data);
}

struct pixel* get_pixel_row(struct image const* image, const uint64_t row_number){
    return &(image->data[row_number*image->width]);
}
