#include "rotate.h"

struct image rotate( struct image const source ){
    struct image changed_image = create_image(source.height, source.width);
    for (uint64_t col = 0; col < source.height; col++)
        for (uint64_t row = 0; row < source.width; row++)
            changed_image.data[row * source.height + source.height - col - 1] = source.data[col * source.width + row];
    return changed_image;
}
