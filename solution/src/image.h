#ifndef IMAGE_H
#define IMAGE_H
#include "malloc.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel *data;
};
struct image create_image(uint64_t width, uint64_t height);
void free_image(struct image* image);
struct pixel* get_pixel_row(struct image const* image, const uint64_t row_number);
#endif
